function FAQ() {
    return (
        <section id="faq">
            <div class="container">
                <div class="row px-1">
                    <div class="col-md-6 col-sm-12 col-12">
                        <div class="tulisan align-items-center">
                            <h1>Frequently Asked Question</h1>
                            <h3>Lorem ipsum dolor sit amet, consectetur adipiscing elit</h3>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12 col-12">
                        <div class="btnfaq">
                            <div class="dropdown-list me-2">
                                <button type="button"
                                    class="btn d-flex justify-content-between align-items-center flex-grow-1 bd-highlight btn-light text-dark btn-outline-secondary me-0"
                                    data-bs-toggle="dropdown" aria-expanded="false">
                                    <h1>Apa saja syarat yang dibutuhkan?</h1>
                                    <img src="assets/icon_dropdown.svg" alt="" />
                                </button>
                                <ul class="dropdown-menu">
                                    <h1>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem
                                        ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum
                                        dolor sit amet, consectetur adipiscing elit, sed do eiusmod</h1>
                                </ul>
                                <button type="button"
                                    class="btn d-flex justify-content-between align-items-center flex-grow-1 bd-highlight btn-light text-dark btn-outline-secondary me-0"
                                    data-bs-toggle="dropdown" aria-expanded="false">
                                    <h1>Berapa hari minimal sewa mobil lepas kunci?</h1>
                                    <img src="assets/icon_dropdown.svg" alt="" />
                                </button>
                                <ul class="dropdown-menu">
                                    <h1>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem
                                        ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum
                                        dolor sit amet, consectetur adipiscing elit, sed do eiusmod</h1>
                                </ul>
                                <button type="button"
                                    class="btn d-flex justify-content-between align-items-center flex-grow-1 bd-highlight btn-light text-dark btn-outline-secondary me-0"
                                    data-bs-toggle="dropdown" aria-expanded="false">
                                    <h1>Berapa hari sebelumnya sabaiknya booking sewa mobil?</h1>
                                    <img src="assets/icon_dropdown.svg" alt="" />
                                </button>
                                <ul class="dropdown-menu">
                                    <h1>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem
                                        ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum
                                        dolor sit amet, consectetur adipiscing elit, sed do eiusmod</h1>
                                </ul>
                                <button type="button"
                                    class="btn d-flex justify-content-between align-items-center flex-grow-1 bd-highlight btn-light text-dark btn-outline-secondary me-0"
                                    data-bs-toggle="dropdown" aria-expanded="false">
                                    <h1>Apakah Ada biaya antar-jemput?</h1>
                                    <img src="assets/icon_dropdown.svg" alt="" />
                                </button>
                                <ul class="dropdown-menu">
                                    <h1>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem
                                        ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum
                                        dolor sit amet, consectetur adipiscing elit, sed do eiusmod</h1>
                                </ul>
                                <button type="button"
                                    class="btn d-flex justify-content-between align-items-center flex-grow-1 bd-highlight btn-light text-dark btn-outline-secondary me-0"
                                    data-bs-toggle="dropdown" aria-expanded="false">
                                    <h1>Bagaimana jika terjadi kecelakaan</h1>
                                    <img src="assets/icon_dropdown.svg" alt="" />
                                </button>
                                <ul class="dropdown-menu">
                                    <h1>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem
                                        ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum
                                        dolor sit amet, consectetur adipiscing elit, sed do eiusmod</h1>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    )
}

export default FAQ;