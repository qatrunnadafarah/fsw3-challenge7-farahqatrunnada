import { useState, useEffect } from 'react';


const axios = require('axios').default;


function Card({ cars }) {

    const [error, setError] = useState(null);

    const [data, setData] = useState([]);
    // const [error, setError] = useState("");
    // const [cars, setData] = useState();

    // const apiGet = (cars) => {
    //     fetch('https://raw.githubusercontent.com/fnurhidayat/probable-garbanzo/main/data/cars.min.json')
    //         .then((response) => response.json())
    //         .then((json) => {
    //             console.log(json);
    //             setData(json);
    //         })
    // }

    // useEffect(() => apiGet(), []);



    useEffect(() => {   
        axios
            .get("https://raw.githubusercontent.com/fnurhidayat/probable-garbanzo/main/data/cars.min.json")
            .then(result => {
                setData(result.data);
                console.log(result);
            }).catch((err) => setError(err.message));
    })





    return (
        <section className="container" style={{ width: '1055px' }}>
            <div className="row d-flex justify-content-between align-content-between flex-wrap" id="hasil-pencarian">
                {data
                    && data.map(cars => (
                        <div className="col-lg-4" >
                            <div className="card">
                                <img src={cars.image} className="card-img-top" alt="..." />
                                <div className="card-body p-0" style={{ height: 'max-content' }}>
                                    <h2 className="card-title">Nama/Tipe Mobil</h2>
                                    <h3>Rp {cars.rentPerDay} / hari</h3>
                                    <p className="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
                                        tempor incididunt ut labore et dolore magna aliqua.</p>
                                    <div>
                                        <div className="d-flex flex-row" style={{ margin: '8px 0', fontSize: '14px' }}>
                                            <img src="assets/fi_users.svg" alt=""
                                                style={{ width: '20px', height: 'auto', margin: '0 8px 0 0', color: '#8A8A8A' }} />
                                            {cars.capacity}
                                        </div>
                                        <div className="d-flex flex-row" style={{ margin: '8px 0', fontSize: '14px' }}>
                                            <img src="assets/fi_settings.svg" alt=""
                                                style={{ width: '20px', height: 'auto', margin: '0 8px 0 0' }} />
                                            {cars.transmission}
                                        </div>
                                        <div className="d-flex flex-row" style={{ margin: '8px 0', fontSize: '14px' }}>
                                            <img src="assets/fi_calendar.svg" alt=""
                                                style={{ width: '20px', height: 'auto', margin: '0 8px 0 0' }} />
                                            {cars.year}
                                        </div>
                                    </div>
                                    <a href="#"
                                        className="btn btn-primary d-flex align-items-center justify-content-center text-center">Pilih
                                        Mobil</a>
                                </div>
                            </div>
                        </div>
                    ))}
            </div>
        </section>
    )
}

export default Card;