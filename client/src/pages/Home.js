import { NavigationBar, LandingPage, Service, WhyUs, GetStart, FAQ, Footer, Testimonial } from '../components';

function Home() {
    return (
        <div className='sewa'>
            <NavigationBar />
            <LandingPage />
            <Service />
            <WhyUs />
            <Testimonial />
            <GetStart />
            <FAQ />
            <Footer />
        </div>
    )
}

export default Home;