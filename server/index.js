const express = require("express");
const cors = require("cors");
const jwt = require("jsonwebtoken");
const app = express();

app.use(cors());
app.use(express.json());

app.get("/", (req, res) => {
  res.send({
    message: "Berhasil",
  });
});

app.post("/cars", async (req, res) => {
  const cars = (await axios.get("https://raw.githubusercontent.com/fnurhidayat/probable-garbanzo/main/data/cars.min.json")).data;
  res.send(cars);  
});

app.listen(3001, () => {
  console.log("Listening on port", 3001);
});
