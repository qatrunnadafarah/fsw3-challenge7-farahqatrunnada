function Testimonial() {
    return (
        <section id="testimonial">
            <div className="row">
                <div className="col-md-12 col-sm-12 col-12">
                    <div id="carousel" className="">
                        <div id="carousel-title" className="text-center d-flex row justify-content-center">
                            <h3>Testimonial</h3>
                            <h4>Berbagai review positif dari para pelanggan kami</h4>
                        </div>
                        <div id="carouselExampleControls" className="carousel slide d-flex justify-content-center"
                            data-bs-ride="carousel">
                            <div className="carousel-inner" style={{fontSize: '14px'}}>
                                <div className="carousel-item active">
                                    <div className="carousel-body">
                                        <img src="assets/img_photo.svg" alt="" />
                                        <div className="carousel-text">
                                            <img src="assets/Rate.svg" className="m-0"
                                                style={{width: '80px', height: 'auto', paddingBottom: '8px'}} alt="" />
                                            <p style={{width: '90%', marginBottom: '8'}}>“Lorem ipsum dolor sit amet,
                                                consectetur
                                                adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur
                                                adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur
                                                adipiscing elit, sed do eiusmod”</p>
                                            <p style={{fontWeight: 'bolder', margin: 0}}>John Dee 32, Bromo</p>
                                        </div>
                                    </div>
                                </div>
                                <div className="carousel-item">
                                    <div className="carousel-body">
                                        <img src="assets/img_photo.svg" alt="" />
                                        <div className="carousel-text">
                                            <img src="assets/Rate.svg" className="m-0"
                                                style={{width: '80px', height: 'auto', paddingBottom: '8px'}} alt="" />
                                            <p style={{fontWeight: 'bolder', margin: 0}}>“Lorem ipsum dolor sit amet,
                                                consectetur
                                                adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur
                                                adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur
                                                adipiscing elit, sed do eiusmod”</p>
                                            <p style={{fontWeight: 'bolder', margin: 0}}>John Dee 32, Bromo</p>
                                        </div>
                                    </div>
                                </div>
                                <div className="carousel-item">
                                    <div className="carousel-body">
                                        <img src="assets/img_photo.svg" alt="" />
                                        <div className="carousel-text">
                                            <img src="assets/Rate.svg" className="m-0"
                                                style={{width: '80px', height: 'auto', paddingBottom: '8px'}} alt="" />
                                            <p style={{fontWeight: 'bolder', margin: 0}}>“Lorem ipsum dolor sit amet,
                                                consectetur
                                                adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur
                                                adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur
                                                adipiscing elit, sed do eiusmod”</p>
                                            <p style={{fontWeight: 'bolder', margin: 0}}>John Dee 32, Bromo</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-12">
                        <div className="position-absolute start-50 d-flex justify-content-around">
                            <div className="tombol">
                                <button className="kanan button carousel-control-prev" type="button"
                                    data-bs-target="#carouselExampleControls" data-bs-slide="next">
                                    <img src="assets/Right_button.svg" alt="" />
                                </button>
                                <button className="kiri button carousel-control-next" type="button"
                                    data-bs-target="#carouselExampleControls" data-bs-slide="prev">
                                    <img src="assets/Left_button.svg" alt="" />
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    )
}

export default Testimonial;