function WhyUs() {
    return (
        <section id="why-us">
            <div className="container" id="hal3">
                <h3>Why Us?</h3>
                <h4>Mengapa harus pilih Binar Car Rental?</h4>
                <div className="row">
                    <div className="col-lg-3 col-md-12 col-sm-12 col-12">
                        <div className="card">
                            <img src="assets/icon_complete.svg" className="card-img-top" alt="..."/>
                                <div className="card-body p-0">
                                    <h5 className="card-title fw-bold ">Mobil Lengkap</h5>
                                    <p className="card-text">Tersedia banyak pilihan mobil, kondisi masih baru, bersih dan
                                        terawat</p>
                                </div>
                        </div>
                    </div>
                    <div className="col-lg-3 col-md-12 col-sm-12 col-12">
                        <div className="card">
                            <img src="assets/icon_price.svg" className="card-img-top" alt="..."/>
                                <div className="card-body p-0">
                                    <h5 className="card-title fw-bold">Harga Murah</h5>
                                    <p className="card-text">Harga murah dan bersaing, bisa bandingkan harga kami dengan
                                        rental mobil lain</p>
                                </div>
                        </div>
                    </div>
                    <div className="col-lg-3 col-md-12 col-sm-12 col-12">
                        <div className="card">
                            <img src="assets/icon_24hrs.svg" className="card-img-top" alt="..."/>
                                <div className="card-body p-0">
                                    <h5 className="card-title fw-bold">Layanan 24 Jam</h5>
                                    <p className="card-text">Siap melayani kebutuhan Anda selama 24 jam nonstop. Kami juga
                                        tersedia di akhir minggu</p>
                                </div>
                        </div>
                    </div>
                    <div className="col-lg-3 col-md-12 col-sm-12 col-12">
                        <div className="card">
                            <img src="assets/icon_professional.svg" className="card-img-top" alt="..."/>
                                <div className="card-body p-0">
                                    <h5 className="card-title fw-bold">Sopir Profesional</h5>
                                    <p className="card-text ">Sopir yang profesional, berpengalaman, jujur, ramah dan selalu
                                        tepat waktu</p>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    )
}

export default WhyUs;