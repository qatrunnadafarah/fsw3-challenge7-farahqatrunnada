function Footer() {
    return (
        <section id="footer">
            <div className="container" style={{marginTop: '150px'}}>
                <div className="row">
                    <div className="col-lg-3">
                        <div className="content flex-fill bd-highlight fw-light">
                            <p>Jalan Suroyo No. 161 Mayangan Kota Probolonggo 672000</p>
                            <p>binarcarrental@gmail.com</p>
                            <p>081-233-334-808</p>
                        </div>
                    </div>
                    <div className="col-lg-3">
                        <div className="content flex-fill bd-highlight fw-bold">
                            <p>Our services</p>
                            <p>Why Us</p>
                            <p>Testimonial</p>
                            <p>FAQ</p>
                        </div>
                    </div>
                    <div className="col-lg-3">
                        <div className="content flex-fill bd-highlight fw-lighter">
                            <p>Connect with us</p>
                            <img src="assets/list_item.svg" alt=""/>
                        </div>
                    </div>
                    <div className="col-lg-3">
                        <div className="content logo me-auto flex-fill bd-highlight fw-lighter">
                            <p className="pt-3">Copyright Binar 2022</p>
                            <img id="logo-footer" src="assets/logo.png" alt="logo" style={{width: '100px'}}/>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    )
}

export default Footer;