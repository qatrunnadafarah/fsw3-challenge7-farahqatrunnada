function Service() {
    return (
        <section id="our-service" className="our-service">
            <div className="container">
                <div className="div row">
                    <div className="col-lg-6 col-sm-12">
                        <img className="girl" src="assets/img_service.png" alt=""/>
                    </div>
                    <div className="col-lg-6 col-sm-12">
                        <h1 className="fw-bold pb-3 pt-4">Best Car Rental for any kind of trip in (Lokasimu)!</h1>
                        <p>Sewa mobil di (Lokasimu) bersama Binar Car Rental jaminan harga lebih murah dibandingkan yang
                            lain, kondisi mobil baru, serta kualitas pelayanan terbaik untuk perjalanan wisata, bisnis,
                            wedding, meeting, dll.</p>
                        <ul className="p-0">
                            <li className="liwhy"><img className="me-4" src="assets/Group_53.png"/>Mobil Dengan Supir di Bali 12 Jam</li>
                            <li className="liwhy"><img className=" me-4" src="assets/Group_53.png"/>Sewa Mobil Lepas Kunci di Bali 24 Jam
                            </li>
                            <li className="liwhy"><img className=" me-4" src="assets/Group_53.png"/>Sewa Mobil Jangka Panjang Bulanan</li>
                            <li className="liwhy"><img className=" me-4" src="assets/Group_53.png"/>Gratis Antar - Jemput Mobil di Bandara
                            </li>
                            <li className="liwhy"><img className=" me-4" src="assets/Group_53.png"/>Layanan Airport Transfer / Drop In Out
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
    )
}

export default Service;