function GetStart() {
    return (
        <section id="get-start">
            <div className="sewabox container">
                <div id="btnsw" className="text-center">
                    <h1 className="text-white">Sewa Mobil di (Lokasimu) Sekarang</h1>
                    <h2 className="text-white">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                        eiusmod tempor incididunt ut labore et dolore magna aliqua.</h2>
                    <button type="button" className="btn text-white" style={{borderRadius: '3px'}}>Mulai Sewa Mobil</button>
                </div>
            </div>
        </section>
    )
}

export default GetStart;