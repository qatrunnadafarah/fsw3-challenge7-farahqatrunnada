import { Link } from 'react-router-dom';

function NavigationBar() {
    return (
        <section>
            <div className="row">
                <div className="div col-lg-12 col-sm-12">
                    <nav className="navbar navbar-expand-lg navbar-expand-md  navbar-light">
                        <div className="container">
                            <div>
                                <Link to='/'>
                                    <a className="ps-0" href="#hal1">
                                        <img id="logo" src="assets/logo.png" className="d-flex align-items-center align-self-center"
                                            alt="" />
                                    </a>
                                </Link>
                            </div>
                            <button className="navbar-toggler" type="button" data-bs-toggle="offcanvas"
                                data-bs-target="#offcanvasNavbar" aria-controls="offcanvasNavbar" aria-expanded="false"
                                aria-label="Toggle navigation">
                                <span className="navbar-toggler-icon"></span>
                            </button>
                            <div className="offcanvas offcanvas-end" tabIndex="-1" id="offcanvasNavbar"
                                aria-labelledby="offcanvasNavbarLabel">
                                <div className="offcanvas-header">
                                    <h5 className="offcanvas-title fw-bold" id="offcanvasNavbarLabel">BCR</h5>
                                    <button type="button" className="btn-close text-reset" data-bs-dismiss="offcanvas"
                                        aria-label="Close"></button>
                                </div>
                                <div className="offcanvas-body">
                                    <ul className="navbar-nav">
                                        <li className="linv"><a href="#our-service">Our Services</a></li>
                                        <li className="linv"><a href="#why-us">Why Us</a></li>
                                        <li className="linv"><a href="#testimonial">Testimonial</a></li>
                                        <li className="linv"><a href="#faq">FAQ</a></li>
                                        <li className="linv"><a id="register" className="active text-white" href="">Register</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </nav>
                </div>
            </div>
        </section>
    )
}

export default NavigationBar;




{/* <nav className="navbar navbar-expand-lg navbar-expand-md navbar-light">
    <Container fluid>
        <Navbar.Brand href="#hal1">
            <img id="logo" src="assets/logo.png" className="d-flex align-items-center align-self-center"
                alt="" />
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="navbarScroll" />
        <Navbar.Collapse id="navbarScroll">
            <Nav className="d-flex" navbarScroll>
                <Nav.Link href="#our-service">Our Services</Nav.Link>
                <Nav.Link href="#why-us">Why Us</Nav.Link>
                <Nav.Link href="#testimonial">Testimonial</Nav.Link>
                <Nav.Link href="#faq">FAQ</Nav.Link>
                <Nav.Link id="register" className="active text-white" href="">Register</Nav.Link>

            </Nav>
        </Navbar.Collapse> */}