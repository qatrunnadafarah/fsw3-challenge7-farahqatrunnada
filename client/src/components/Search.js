// import DatePicker from "react-date-picker";
// import { Form } from 'react-bootstrap'
// import { useSelector, useDispatch } from "react-redux";
// import { fetchUsers } from "../redux";

function Search() {

    // const [driver, setDriver] = useState("Pilih Tipe Driver");
    // const [date, setDate] = useState("Pilih Waktu");
    // const [pickupTime, setPickupTime] = useState("8");
    // const [passenger, setPassenger] = useState("");
    // const dispatch = useDispatch();
    // const state = useSelector((state) => state);


    // const handleSubmit = (e) => {
    //     e.preventDefault();
    // if (driver !== "Pilih Tipe Driver") {
    // const pass = passenger ? passenger : "0";
    // const filter = { driver, date, pickupTime, pass };
    // dispatch(fetchUsers(filter));
    //     }
    // };

    // const handleDriver = (e) => {
    //     setDriver(e.target.value);
    // };
    // const handleDate = (e) => {
    //     setDate(e.target.value);
    // };
    // const handlePickupTime = (e) => {
    //     setPickupTime(e.target.value);
    // };
    // const handlePassenger = (e) => {
    //     setPassenger(e.target.value);
    // };

    // useEffect(() => {
    //     console.log(state.cars);
    // }, [state]);

    return (
        <section id="cari-mobil" className="text-center ">
            <div className="daftar-box d-flex row justify-content-center">
                <div className="row">
                    <div className="daftar col-lg-2 d-flex flex-column align-items-start">
                        <p className="mb-1">Tipe Driver</p>
                        <div className="col-md">
                            <div className="form-floating d-flex align-items-center m-0">
                                <div id="dropdown1 p-0">
                                    <select className="form-select daftar-driver drop" id="floatingSelectGrid">
                                        <option>
                                            <h1 style={{ color: '#8A8A8A' }}>Pilih Tipe Driver</h1>
                                        </option>
                                        <option value="1" className="dropdown-item" style={{ fontSize: '12px' }}>Dengan Sopir
                                        </option>
                                        <option value="2" className="dropdown-item" style={{ fontSize: '12px' }}>Tanpa Sopir (Lepas
                                            Kunci)</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="daftar col-lg-2 d-flex flex-column align-items-start" style={{ paddingLeft: '20px', paddingRight: '20px' }}>
                        <p className="mb-1">Tanggal</p>
                        <div className="dropdown daftar m-0">
                            <div className="mb-3 d-flex flex-row">
                                <input type="date" className="form-control daftar d-flex justify-content-between"
                                    id="exampleInputEmail1" />
                            </div>
                        </div>
                    </div>
                    <div className="daftar col-lg-2 d-flex flex-column align-items-start"
                        style={{ marginLeft: '12px', paddingLeft: '16px', paddingRight: '16px' }}>
                        <p className="mb-1">Waktu Jemput/Ambil</p>
                        <div className="col-md">
                            <div className="form-floating d-flex align-items-center m-0">
                                <div id="dropdown1 p-0" className="d-flex flex-row">
                                    <select className="form-select daftar-driver" id="floatingSelectGridTime">
                                        <option>
                                            <h1 style={{ color: '#8A8A8A' }}>Pilih Waktu</h1>
                                        </option>
                                        <option value="1" className="dropdown-item" style={{ fontSize: '12px' }}>
                                            <p className="m-0">08.00</p>
                                            <p className="m-0">WIB</p>
                                        </option>
                                        <option value="2" className="dropdown-item" style={{ fontSize: '12px' }}>
                                            <p className="m-0">09.00</p>
                                            <p className="m-0">WIB</p>
                                        </option>
                                        <option value="2" className="dropdown-item" style={{ fontSize: '12px' }}>
                                            <p className="m-0">10.00</p>
                                            <p className="m-0">WIB</p>
                                        </option>
                                        <option value="2" className="dropdown-item" style={{ fontSize: '12px' }}>
                                            <p className="m-0">11.00</p>
                                            <p className="m-0">WIB</p>
                                        </option>
                                        <option value="2" className="dropdown-item" style={{ fontSize: '12px' }}>
                                            <div className="dropdown-item time" style={{ fontSize: '12px' }}>
                                                <p className="m-0">12.00</p>
                                                <p className="m-0">WIB</p>
                                            </div>
                                        </option>
                                    </select>
                                    <img id="penumpang" src="assets/fi_clock_1.svg"
                                        style={{ color: '#8A8A8A', width: '18px', height: 'auto' }} alt="" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="dropdown daftar col-lg-6 d-flex flex-grow-1" style={{ marginLeft: '12px', paddingLeft: '13px', paddingRight: '13px' }}>
                        <div className="d-flex flex-column align-items-start">
                            <p className="mb-1">Jumlah Penumpang (optional)</p>
                            <div className="daftar m-0 d-flex flex-row">
                                <input type="text" placeholder="Jumlah Penumpang" id="jmlPenumpang" className="form-control daftar text-dark m-0" aria-label="default input example">
                                </input>
                                <img id="penumpang" src="assets/fi_users.svg"
                                    style={{ color: '#8A8A8A', width: '18px', height: 'auto' }} alt="" />
                            </div>

                        </div>
                    </div>
                    <button type="button" className="btn align-items-center" id="cari" style={{ borderRadius: "3px" }}>
                        <p className="d-flex align-items-end align-self-center m-0" style={{ width: 'max-content' }}>Cari Mobil
                        </p>
                    </button>
                </div>
            </div>
        </section>
    )
}

export default Search;