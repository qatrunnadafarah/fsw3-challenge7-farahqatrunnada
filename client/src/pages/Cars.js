
// import { useSelector, useDispatch } from "react-redux";
import { NavigationBar, LandingPageCars, Footer, Search, Card } from '../components';

function Cars() {

    // const state = useSelector((state) => state);

    // useEffect(() => {
    //     console.log(state.cars);
    // }, [state]);

    return (
        <div className='sewa'>
            <NavigationBar />
            <LandingPageCars /> 
            <Search />
            {/* <Card cars={state.cars}/> */}
            <Card />
            <Footer />
        </div>
    )
}

export default Cars;