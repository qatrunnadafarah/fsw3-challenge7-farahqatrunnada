import { Link } from 'react-router-dom';
 
function LandingPage() {
    return (
        <section className="sec1">
            <div id="landing-page" class="container">
                <div class="row">
                    <div id="hero-section" className="col-lg-6 col-sm-12 col-12 align-self-center">
                        <h1 className="pb-3">Sewa & Rental Mobil Terbaik di kawasan (Lokasimu)</h1>
                        <h4 style={{ width: "75%" }}>Selamat datang di Binar Car Rental. Kami menyediakan
                            mobil kualitas terbaik dengan harga terjangkau. Selalu siap melayani kebutuhanmu untuk sewa
                            mobil selama 24 jam.</h4>
                        <Link to="/cars">
                        <button className="btn" style={{ marginTop: "16px" }}>
                            Mulai Sewa Mobil
                        </button>
                        </Link>
                    </div>
                    <div className="col-lg-6 col-sm-12 col-12 p-0">
                        <img class="car" src="assets/img_car.png" alt="" />
                    </div>
                </div>
            </div>
        </section>
    )
}

export default LandingPage;